// Copyright Epic Games, Inc. All Rights Reserved.


#include "Snake_GameGameModeBase.h"

ASnake_GameGameModeBase::ASnake_GameGameModeBase()
{
	PrimaryActorTick.bCanEverTick = true;

}

void ASnake_GameGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnake_GameGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	// ��������� ������
	FTimerHandle SpawnTimer;
	// ������� ������ ��� ������
	GetWorld()->GetTimerManager().SetTimer(SpawnTimer, this, &ASnake_GameGameModeBase::SpawnSomething, 2.0f, true);
}

// ������� ��� ������ ��������� ������� �� �������
void ASnake_GameGameModeBase::SpawnSomething()
{
	if (SpawnActorArray.Num())
	{
		int32 RandomIndex = FMath::RandRange(0, SpawnActorArray.Num() - 1);
		auto ClassForSpawn = SpawnActorArray[RandomIndex];

		int32 RandomX = FMath::RandRange(-478, 482);
		int32 RandomY = FMath::RandRange(-840, 840);

		auto TempBonus = GetWorld()->SpawnActor<ABonus>(ClassForSpawn, FTransform(FVector(RandomX, RandomY, 0)));

		//����������� ����� �������� ����� 10 ���
		TempBonus->SetLifeSpan(5.f);

	}
}
