// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus.h"

#include "SnakeBase.h"



ABonus::ABonus()
{
	PrimaryActorTick.bCanEverTick = true;
	BnSpeed = false;
	BnLife = false;
	BnBlock = false;
	BnFood = false;
	BnPower = 0.1f;
}

void ABonus::BeginPlay()
{
	Super::BeginPlay();

}

void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonus::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		
		if (bIsHead)
		{
		
			if (BnLife == true)
			{
				Snake->ChangeLife(1);
			}

			if (BnSpeed == true)
			{
				Snake->SetActorTickInterval(Snake->MovementSpeed -= BnPower);
			}

			if (BnBlock == true)
			{
				Snake->ChangeLife(-1);
			}
			
			if (BnFood == true)
			{
				Snake->AddSnakeElement();
			}

		}

		Destroy();
	}
}

