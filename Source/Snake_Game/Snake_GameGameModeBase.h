// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Bonus.h"
#include "Snake_GameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_GAME_API ASnake_GameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ASnake_GameGameModeBase();

	UPROPERTY(EditDefaultsOnly)
	TArray<TSubclassOf<ABonus>> SpawnActorArray;

	virtual void Tick(float DeltaTime) override;

	void SpawnSomething();

protected:
	virtual void BeginPlay() override;

};
